import 'package:flutter/material.dart';

import '../widgets/category_item.dart';
import '../dummy_data.dart';
import '../Models/category.dart';

class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       backgroundColor: Colors.white,
        body: GridView(
      children: DUMMY_CATEGORIES.map((catData) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: CategoryItem(
            catData.id,
            catData.title,
            catData.color,
          ),
        );
      }).toList(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 3 / 2,
        mainAxisSpacing: 20,
        crossAxisSpacing: 20,
      ),
    ));
  }
}
